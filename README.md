# cl-subversion

CFFI bindings for the subversion libraries. Currently (part of) the RA layer is 
supported, for direct repository access (across http, https, svn, svn+ssh, and 
file protocols).


# Directory traversal and path names

*PLEASE NOTE* that here is a major point where this library differs from the 
C API: while the C API requires you to pass paths relative from the edit 
root, the *highlevel* API takes care about that and allows you to use names 
relative to the current directory. (The `svn/api`-layer is only a translation 
and so needs "full" paths, *without* a starting slash!)

You can switch that around via the `:relative-to-base` argument to `add-file`, 
`add-directory`, and the `with-directory` macros - `NIL` means 'relative to 
current directory' (default), `T` means 'relative to edit root', and 
`:slash-means-base` says that if the path has a `/` at the beginning it's 
relative to the edit base, else to the current directory.


# Usage examples

Please see the test scripts in `test/`. I recommend using the high-level API.


# Re-initializing from a dumped image

While loading `CL-SUBVERSION` will initialize the foreign libraries (`libsvn`, 
`libapr`, etc.) already, when running a dumped image you'll have to ensure that 
the libraries are initialized yourself. Use the `INITIALIZE` function, 
available both in the API as well as the high-level package.


# Memory usage

Due to necessarily using the `apr` memory pools you'll see memory usage growing 
even with a fixed CL heap.

The `with-apr-pool` macro will destroy the pools after use; but some functions 
need to use `*global-apr-pool*` which won't be freed automatically.

