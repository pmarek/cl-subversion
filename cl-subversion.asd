(cl:in-package #:asdf-user)

#+quicklisp
(ql:quickload '(:alexandria
                 :iterate
                 :flexi-streams
                 :fiveam))

(asdf:defsystem :cl-subversion
  :description "FFI bindings to the subversion libraries."
  :license "LLGPL"
  :author "Philipp Marek <philipp@marek.priv.at>"
  :depends-on (:iterate
                  :alexandria
                  :cffi)
  :serial t
  :components ((:module "src"
                        :serial T
                        :components
                        ((:file "package")
                         (:file "ffi")
                         (:file "svn-string")
                         (:file "apr")
                         (:file "svn-error")
                         (:file "svn-dirent-uri")
                         (:file "svn-delta")
                         (:file "file-deltas")
                         (:file "svn-stream")
                         (:file "svn-ra")
                         (:file "svn-repos")
                         (:file "svn")
                         (:file "highlevel")
                         ))))



(asdf:defsystem :cl-subversion/test
  :description "Testing CL-SUBVERSION"
  :license "LLGPL"
  :author "Philipp Marek <philipp@marek.priv.at>"
  :depends-on (:cl-subversion
                :fiveam
                :osicat)
  :components ((:module "test"
                        :serial T
                        :components
                        ((:file "test-pkg")
                         (:file "test-basics")
                         (:file "test-api")
                         (:file "test-hl")
                         ))))
