(in-package :cl-subversion/apr)


(cffi:defctype apr-pool :pointer)

(cffi:defcfun ("apr_initialize" apr-initialize) :int)

(cffi:defcfun ("apr_pool_clear" apr-pool-clear) :void
  (pool apr-pool))

(cffi:defcfun ("apr_pool_destroy" apr-pool-destroy) :void
  (pool apr-pool))

;; TODO: define types?
;; (cffi:defctype apr-pool :pointer)

(cffi:defcfun ("apr_pool_create_ex" apr_pool_create_ex%) :int
  (newpool (:pointer apr-pool))
  (oldpool apr-pool)
  (abort_fn :pointer)
  (allocator :pointer))


(defun apr-pool-create (&optional parent)
  (with-pointer-to-type/return (apr-pool new-pool)
    (assert (zerop (apr_pool_create_ex% new-pool
                                        (or parent (cffi:null-pointer))
                                        (cffi:null-pointer)
                                        (cffi:null-pointer))))))


(defmacro with-apr-pool ((var &optional parent)
                         &body body)
  `(let ((,var (apr-pool-create (or ,parent 
                                    (cffi:null-pointer)))))
     (unwind-protect 
         (progn
           ,@ body)
       (apr-pool-destroy ,var))))


(cffi:defctype apr-hash :pointer)

(cffi:defcfun ("apr_hash_make" apr-hash-make) apr-hash
  (pool :pointer))

(cffi:defcfun ("apr_hash_set" apr-hash-set) :void
  (hash apr-hash)
  (key :pointer)
  (keylen :long)
  (val :pointer))


(defun hash-to-aprhash (ht pool)
  "Converts a hash-table or an ALIST to an apr hash."
  (typecase ht
    (null
      (cffi:null-pointer))
    ;; we need to copy the key to guarantee the lifetime?
    (hash-table
      (let ((ah (apr-hash-make pool)))
        (iterate (for (k v) in-hashtable ht)
            (check-type k string) 
            (check-type v string) 
            (apr-hash-set ah
                          (apr-pstrdup-as-pointer pool k)
                          (babel:string-size-in-octets k)
                          (svn-string-create v pool)))
        ah))
    (cons
      (let ((ah (apr-hash-make pool)))
        (iterate (for (k . v) in ht)
            (check-type k string) 
            (check-type v string) 
            (apr-hash-set ah
                          (apr-pstrdup-as-pointer pool k)
                          (babel:string-size-in-octets k)
                          (svn-string-create v pool)))
        ah))
    (T
      ht)))



(cffi:defctype apr-hash-index-t :pointer)

(cffi:defcfun ("apr_hash_first" apr-hash-first%) apr-hash-index-t
  (pool :pointer)
  (apr-hash apr-hash))

(defun apr-hash-first (ht pool)
  (let ((f (apr-hash-first% ht pool)))
    (if (cffi:null-pointer-p f)
        nil
        f)))

(cffi:defcfun ("apr_hash_next" apr-hash-next%) apr-hash-index-t
  (iterator apr-hash-index-t))

(defun apr-hash-next (it)
  (let ((n (apr-hash-next% it)))
    (if (cffi:null-pointer-p n)
        nil
        n)))

;; Actually, this is a (void*), and has a corresponding "_len" 
;; function as well; but SVN property names are (char*) only
(cffi:defcfun ("apr_hash_this_key" apr-hash-this-key) :pointer
  (iterator apr-hash-index-t))
(cffi:defcfun ("apr_hash_this_key_len" apr-hash-this-key-len) :long
  (iterator apr-hash-index-t))
(cffi:defcfun ("apr_hash_this_val" apr-hash-this-value) :pointer
  (iterator apr-hash-index-t))


(cffi:defcfun ("apr_hash_get" apr-hash-get-string%) :pointer
  (hash apr-hash)
  (key :string) ; actually a :pointer, but normally a string is used
  (keylen :long))

(defun apr-hash-get (hash key &optional keylen)
  "Asks for KEY in HASH; no KEYLEN (or -1) means to get the string length."
  (apr-hash-get-string% hash 
                        key
                        (or keylen (length key))))



(cffi:defcfun ("apr_pstrdup" apr-pstrdup-as-pointer) :pointer
  (pool apr-pool)
  (input :string))


(defmacro do-apr-hash ((hash pool key-var value-var &key key-len-var) &body body)
  "Iterates across all elements of HASH, allocating the iterator in POOL.
  KEY-VAR and VALUE-VAR are set within BODY.
  VALUE-VAR will be a SVN_STRING_T, mostly."
  (with-gensyms (iterator)
    `(iter 
       (for ,iterator initially (apr-hash-first ,pool ,hash)
            then (apr-hash-next ,iterator))
       (while ,iterator)
       (let ((,key-var (apr-hash-this-key ,iterator))
             ,@ (if key-len-var
                    `((,key-len-var (apr-hash-this-key-len ,iterator))))
             (,value-var (apr-hash-this-value ,iterator)))
         ,@ body))))


(defun aprhash-to-alist (aprhash pool &key 
                                 (trust-key-len T)
                                 (value-builder #'cl-subversion/ffi:svn-string-to-lisp))
  "Converts an apr hash to an ALIST."
  (let ((result ()))
    (do-apr-hash (aprhash pool key val :key-len-var k-l)
      (push 
        (cons (cffi:foreign-string-to-lisp key 
                                           :count (if trust-key-len k-l))
              (funcall value-builder val))
        result))
    result))
