(in-package :cl-subversion/ffi)


(defmacro with-pointer-to-type ((type name &key 
                                      (data (gensym "DATA-"))
                                      (getter (gensym "GETTER-")))
                                &body body)
  "Allocates TYPE and a pointer NAME to it."
  `(cffi:with-foreign-object (,data ,type)
     (cffi:with-foreign-object (,name :pointer)
       (flet ((,getter ()
                (cffi:mem-ref ,name ,type)))
         (setf (cffi:mem-ref ,name :pointer) 
               ,data)
         (progn
           ,@ body)))))


(defmacro with-pointer-to-type/return ((type name &optional (getter (gensym "GETTER-")))
                                       &body body)
  "Allocates TYPE and a pointer NAME to it, and returns the content after running BODY."
  `(with-pointer-to-type (',type ,name :getter ,getter)
     ,@ body
     (,getter)))
