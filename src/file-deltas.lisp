;;; SVN txdelta handling
;;;
;;; SVN uses delta encoding for transferring files to the repository.

(in-package :cl-subversion/api)

(defstruct txdelta-handler-data
  (handler)
  (h-baton)
  (h-pool))


(defun apply-textdelta (file-baton result-pool &key hex-checksum)
  "Fetches a txdelta window handler for FILE-BATON."
  (with-slots (editor e-baton e-pool) *svn-delta-editor-data*
    (cl-subversion/ffi:with-pointer-to-type (:pointer handler-baton
                                                      :getter get-baton)
      (cl-subversion/ffi:with-pointer-to-type (:pointer window-handler
                                                        :getter get-handler)
        (cffi:foreign-funcall-pointer (struct-svn-delta-editor-apply_textdelta editor)
                                      ()
                                      :pointer file-baton
                                      :string (or hex-checksum
                                                  (cffi:null-pointer))
                                      apr-pool result-pool
                                      :pointer window-handler
                                      :pointer handler-baton
                                      svn-error-type)
        (make-txdelta-handler-data :handler (get-handler)
                                   :h-baton (get-baton)
                                   :h-pool result-pool)))))


(cffi:defcenum svn-delta-action
  +svn-txdelta-source+
  +svn-txdelta-target+
  +svn-txdelta-new+)


(cffi:defcstruct (svn_txdelta_op_t
                   :conc-name δ-op-)
  (action_code svn-delta-action)
  (offset :uint64)
  (length :uint64))


(cffi:defcstruct (svn_txdelta_window_t
                   :conc-name δ-window-)
  (sview_offset :uint64)
  (sview_len :uint64)
  (tview_len :uint64)
  (num_ops :int)
  (src_ops :int)
  (ops :pointer)
  (new_data :pointer))


(cffi:defcfun ("svn_txdelta_send_string" svn_txdelta_send_string%) svn-error-type
  (svn-string-data :pointer)
  (window-handler :pointer)
  (handler-baton :pointer)
  (pool apr-pool))


(cffi:defcfun ("svn_txdelta_send_stream" svn_txdelta_send_stream%) svn-error-type
  (svn-string-data :pointer)
  (window-handler :pointer)
  (handler-baton :pointer)
  (output-digest :string)
  (pool apr-pool))


(defmethod send-file-data (txdelta-handler-data (data string))
  (with-slots (handler h-baton h-pool) txdelta-handler-data
    (svn_txdelta_send_string% (svn-string-create data h-pool)
                              handler
                              h-baton
                              h-pool)))


(defun call-txdelta-handler-with-window (txdelta window)
  (check-type txdelta txdelta-handler-data)
  (with-slots (handler h-baton) txdelta
    (cffi:foreign-funcall-pointer handler
                                  ()
                                  :pointer (or window (cffi:null-pointer))
                                  :pointer h-baton
                                  svn-error-type)))


(defun make-window-with-one-copy-op (data data-len data-offset pool
                                          &key encoding)
  (let* ((binary-data 
           (cond 
             ((stringp data)
              (let ((string-bytes (babel:string-to-octets 
                                    data
                                      :encoding (or encoding 
                                                    babel:*default-character-encoding*)
                                      :end data-len)))
                ;; characters can be longer than a single byte
                (setf data-len
                      (babel:string-size-in-octets data))
                string-bytes))
             ((typep data '(array (unsigned-byte 8) *))
              data)
             (t 
              (error "can't handle ~a" (type-of data)))))
         (svn-string (svn-string-create-from-ub8 binary-data pool :len data-len)))
    (cffi:with-foreign-object (op '(:struct svn_txdelta_op_t))
      (setf (δ-op-length      op) data-len
            (δ-op-offset      op) 0 ; data-offset
            (δ-op-action_code op) +svn-txdelta-new+)
      (cffi:with-foreign-object (window '(:struct svn_txdelta_window_t))
        (setf (δ-window-sview_offset window) 0
              (δ-window-sview_len    window) 0
              (δ-window-tview_len    window) data-len
              (δ-window-num_ops      window) 1
              (δ-window-src_ops      window) 0
              (δ-window-ops          window) op
              (δ-window-new_data     window) svn-string)
        window))))
