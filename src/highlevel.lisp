(in-package :cl-subversion/high-level)

(defvar *current-directory-baton* nil)
(defvar *current-file-baton* nil)
(defvar *ra-session* nil)

;; TODO: better pool handling - at least one per session!!
;; We can't simply do recursive pools per directory, though;
;; some results have a longer lifetime than the directory.


;;; While there's "parent_baton" sprinkled around the svn_delta API,
;;; it's not being used as one might believe.
;;;   https://svn.apache.org/repos/asf/subversion/tags/1.10.6/subversion/include/svn_delta.h,
;;;     line 732:
;;;
;;;   The @a path argument to each of the callbacks is relative to the
;;;   root of the edit.  Editors will usually want to join this relative
;;;   path with some base stored in the edit baton (e.g. a URL, or a
;;;   location in the OS filesystem).
;;;
;;; So we need to store the textual path representation as well;
;;; we do so via a hash table on the directory baton.

(defvar *dir-path-to-base* (make-hash-table :test #'eq
                                            :synchronized t
                                            :weakness :key)
  "Directory names, keyed by the baton. All entries end with a slash.")

(defvar *svn-paths-relative-to-base* nil
  "Whether paths in the high-level API are relative to the current directory or not.
  One of NIL, T, or :SLASH-MEANS-BASE.")


(defun path-to-entry (parent-baton name)
  (let ((path-is-relative-to-base
          (if (eq *svn-paths-relative-to-base* :slash-means-base)
            (eql (aref name 0) #\/)
            *svn-paths-relative-to-base*)))
    (if path-is-relative-to-base
      name
      (concatenate 'string 
                   (gethash parent-baton *dir-path-to-base*)
                   name))))

(defun add-dir-path (baton path)
  (setf (gethash baton *dir-path-to-base*)
        (concatenate 'string path "/")))


(defun is-a-valid-baton (val)
  (assert (and val
               (sb-sys:system-area-pointer-p val)
               (not (cffi:null-pointer-p val)))))


(defmacro with-svn-ra-session ((url pool &key 
                                    (session-var '*ra-session*)
                                    config-hash
                                    base-revision
                                    uuid)
                               &body body)
  "Opens an RA session to URL.
  SESSION-VAR is bound in BODY."
  (alexandria:once-only (url pool uuid config-hash)
    `(let ((,session-var (svn-ra-open ,url ,pool
                                      :uuid ,uuid 
                                      :config ,config-hash)))
       ,@ body)))


;; TODO: use ROOT
(defmacro with-svn-commit ((url &key root uuid base-rev parent-pool
                                log-text
                                commit-attr-alist)
                           &body body)
  "Starts a new commit against URL using LOG-TEXT and other properties in 
  COMMIT-ATTR-ALIST; within BODY *SVN-DELTA-EDITOR-DATA* is bound, and the root 
  directory is available in *CURRENT-DIRECTORY-BATON*."
  (declare (ignore root))
  (alexandria:with-gensyms (pool session)
    `(cl-subversion/apr:with-apr-pool (,pool ,parent-pool)
       (let* ((,session (svn/api:svn-ra-open ,url ,pool
                                             :uuid ,uuid)))
         (if (cffi:null-pointer-p ,session)
           (error "Couldn't create an SVN RA session"))
         (svn/api:with-svn-ra-commit-editor (,session ,pool
                                             (cons (cons "svn:log" (or ,log-text "no log text"))
                                                   ,commit-attr-alist))
           (let ((*current-directory-baton* (svn/api:open-root
                                              (or ,base-rev 
                                                  (svn/api:svn-ra-get-latest-revnum ,session ,pool)))))
             ,@ body))))))


(defun new-substructure (form sub-name body)
  `(progn
     (is-a-valid-baton *current-directory-baton*)
     (let ((*current-directory-baton* , form))
       (is-a-valid-baton *current-directory-baton*)
       (add-dir-path *current-directory-baton* ,sub-name)
       (unwind-protect (progn ,@ body)
         (close-directory *current-directory-baton*)))))

(defmacro with-directory ((path &key (parent-baton '*current-directory-baton*) base-rev)
                          &body body)
  "Opens the subdirectory PATH below PARENT-BATON at BASE-REV.
  Binds *CURRENT-DIRECTORY-BATON* for use below."
  (alexandria:once-only (path parent-baton base-rev)
    (alexandria:with-gensyms (sub-path)
      `(let ((,sub-path (path-to-entry ,parent-baton ,path)))
        ,(new-substructure 
          `(open-directory ,sub-path
                           ,parent-baton
                           :base-rev ,base-rev)
          sub-path
          body)))))


(defmacro with-added-directory ((path &key (parent-baton '*current-directory-baton*))
                                &body body)
  "Creates a subdirectory PATH below PARENT-BATON.
  Binds *CURRENT-DIRECTORY-BATON* for use below."
  (once-only (path parent-baton)
    (alexandria:with-gensyms (sub-path)
       `(let ((,sub-path (path-to-entry ,parent-baton ,path)))
         ,(new-substructure 
           `(cl-subversion/api:add-a-directory (path-to-entry ,parent-baton ,path)
                                             ,parent-baton)
           sub-path
           body)))))


(defgeneric change-directory-properties (props &key baton)
  (:documentation 
    "Sets PROPS as new properties on BATON (which defaults to *CURRENT-DIRECTORY-BATON*)."))

(defmethod change-directory-properties ((props-alist list) &key (baton *current-directory-baton*))
  (is-a-valid-baton baton)
  (iter (for (key . value) in props-alist)
    (svn/api:change-dir-prop baton key value))
  baton)

(defgeneric change-file-properties (props &key baton)
  (:documentation 
    "Sets PROPS as new properties on BATON (which defaults to *CURRENT-FILE-BATON*)."))

(defmethod change-file-properties ((props-alist list) &key (baton *current-file-baton*))
  (is-a-valid-baton baton)
  (iter (for (key . value) in props-alist)
    (svn/api:change-file-prop baton key value))
  baton)


(defun add-directory (path &key (parent-baton *current-directory-baton*)
                           props
                           copyfrom-path copyfrom-revnum)
  (is-a-valid-baton parent-baton)
  (let* ((full-path (path-to-entry parent-baton path))
         (baton (cl-subversion/api:add-a-directory full-path parent-baton
                                                :copyfrom-path copyfrom-path
                                                :copyfrom-revnum copyfrom-revnum)))
    (is-a-valid-baton baton)
    (add-dir-path baton full-path)
    (if props
      (change-directory-properties props :baton baton))
    (close-directory baton)
    path))



(defun add-file (name source &key copyfrom-path copyfrom-rev
                      (parent-baton *current-directory-baton*)
                      sending-options
                      props &allow-other-keys)
  "Creates a new file NAME in the given directory."
  (let ((*current-file-baton* 
          (cl-subversion/api:add-a-file (path-to-entry parent-baton name)
                                      parent-baton
                                      :copyfrom-path copyfrom-path
                                      :copyfrom-revnum copyfrom-rev)))
    (is-a-valid-baton *current-file-baton*)
    (if props
      (change-file-properties props))
    ;;
    (when source 
      ;; for SOURCE being NIL that amounts to the same thing as not starting at all
      (with-delta-editor-pool (pool)
        (let ((txdelta-handler (svn/api:apply-textdelta 
                                 *current-file-baton*
                                 pool)))
          (apply #'set-file-data 
                 txdelta-handler
                 source
                 sending-options))))))


(defgeneric set-file-data (txdelta source &key chunk-size)
  (:documentation
  "Changes the contents of BATON to SOURCE.

  If applicable, CHUNK-SIZE says which buffer sizes to send at once.
    
  Bigger data blocks should be cut down, a) to avoid BIG allocations
  for STRING => cstring conversion, and b) to provide progress reports later on. "
  ))

(defmethod set-file-data (txdelta (source null) &key &allow-other-keys)
  ;; Setting to NIL means emptying it;
  ;; deletion is a different function.
  (set-file-data txdelta ""))


(defmethod set-file-data (txdelta (source string) &key
                                  (chunk-size (* 128 1024)) &allow-other-keys)
  ;; TODO start, end arguments?
  (if (and chunk-size
           (> (length source)
              chunk-size))
    (with-input-from-string (s source)
      (set-file-data txdelta s
                     :chunk-size chunk-size))
    (svn/api:send-file-data txdelta source)))


;; TODO: differentiate binary
(defmethod set-file-data (txdelta (source stream) &key 
                                  chunk-size (dest-offset 0)
                                  &allow-other-keys)
  (unless chunk-size
    (setf chunk-size (* 64 1024)))
  (with-delta-editor-pool (pool)
    (iter (with buffer = (make-array chunk-size
                                     :element-type (stream-element-type source)
                                     ;:fill-pointer t
                                     ))
      (for len = (read-sequence buffer source :start 0 :end chunk-size))
      (while (plusp len))
      ;;
      (for window = (svn/api:make-window-with-one-copy-op buffer len dest-offset pool))
      (incf dest-offset len)
      (call-txdelta-handler-with-window txdelta window))
    ;; Close txdelta sending
    (call-txdelta-handler-with-window txdelta nil)))


;; TODO: these query functions are not affected by the commit WITH-DIRECTORY etc. macros
(defun get-file-as-string (name &key revision (element-type 'character))
  "Fetches NAME from the current directory, and returns it either as a 
  STRING. An explicit ELEMENT-TYPE of NIL won't fetch the data.
  The second return value are the properties, the third the fetched revision."
  (let ((fqfn name #+(or)(add-dir-path *current-directory-baton* name))
        (data (make-string-output-stream :element-type element-type)))
    (multiple-value-bind (props rev)
        (svn/api:svn-ra-get-file *ra-session*
                                 fqfn
                                 (or revision +SVN-IGNORED-REVNUM+)
                                 (if element-type 
                                     data))
      (values (get-output-stream-string data)
              props
              rev))))

(defun get-directory (path &key (revision +SVN-IGNORED-REVNUM+))
  "Returns an ALIST of entries, the properties, and the actual revision number."
  (svn/api:svn-ra-get-dir *ra-session*
                          path #+(or) (add-dir-path *current-directory-baton* name)
                          :rev revision))

