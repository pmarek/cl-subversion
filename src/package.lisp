(defpackage :cl-subversion/ffi
  (:use :cl)
  (:export #:svn-string-create
           #:svn-string-create-from-ub8
           #:svn-string-len
           #:svn-string-to-lisp
           #:with-pointer-to-type
           #:with-pointer-to-type/return
           #:svn-error-type
           #:svn-indirect-call
           ))

(defpackage :cl-subversion/apr
  (:use :cl
        :iterate
        :alexandria
        :cl-subversion/ffi)
  (:export #:apr-pool-create
           #:apr-pool-destroy
           #:apr-pool-clear
           #:with-apr-pool
           #:apr-hash-make
           #:apr-hash-get
           #:apr-hash
           #:apr-pool
           #:hash-to-aprhash
           #:aprhash-to-alist
           #:apr-initialize
           #:apr-pstrdup-as-pointer
           ))

(defpackage :cl-subversion/api
  (:nicknames :svn/api)
  (:use :cl
        :iterate
        :alexandria
        :cl-subversion/ffi
        :cl-subversion/apr)
  (:export #:initialize

           #:with-svn-ra-session
           #:with-svn-ra-commit-editor

           #:svn-uri-canonicalize
           #:svn-dirent-canonicalize
           #:svn-ra-open
           #:svn-ra-get-latest-revnum
           #:svn-ra-get-commit-editor3
           #:svn-ra-rev-proplist
           #:svn-cmdline-create-auth-baton

           #:with-delta-editor-pool
           #:call-txdelta-handler-with-window
           #:make-window-with-one-copy-op

           #:open-root

           #:add-a-directory
           #:open-directory
           #:close-directory
           #:change-dir-prop

           #:add-a-file
           #:open-file
           #:close-file
           #:change-file-prop
           #:apply-textdelta
           #:send-file-data

           #:svn-string-create
           #:svn-string-to-lisp
           #:svn-error-case

           #:is-svn-property

           #:only-svn-properties
           #:only-user-properties

           #:svn-ra-get-file
           #:svn-ra-get-dir

           #:svn-repos-create
           #:+fs-type+
           #:+fs-type-bdb+
           #:+fs-type-fsx+
           #:+fs-type-fsfs+

           #:+SVN-IGNORED-REVNUM+
           ))

(defpackage :cl-subversion/high-level
  (:nicknames :svn/highlevel :svn)
  (:use :cl
        :iterate
        :alexandria
        :cl-subversion/apr
        :cl-subversion/api
        :cl-subversion/ffi)
  (:export #:initialize
           #:with-svn-ra-session
           #:with-svn-commit
           #:with-directory
           #:with-added-directory
           #:change-directory-properties
           #:change-file-properties
           #:add-directory
           #:close-directory
           #:add-file
           #:with-file

           #:get-file-as-string
           #:svn-ra-get-file

           #:only-svn-properties
           #:only-user-properties

           #:svn-repos-create
           #:+fs-type+
           #:+fs-type-bdb+
           #:+fs-type-fsx+
           #:+fs-type-fsfs+
           ))
