;;;; Impedance matching Lisp and the subversion delta editor.
;;;
;;;
;;; We could hide everything within a the current environment and make it work 
;;; automagically; the problem is that it's not possible to have one piece of
;;; code work in two repositories at the same time.
;;; To facilitate that we're using a special variable that can be re-bound if
;;; required.
;;;
;;; We're using individual functions so that their arguments can be easily seen;
;;; with locally bound functions that would be harder.
;;;


(in-package :cl-subversion/api)


(defconstant +SVN-IGNORED-REVNUM+ -1)


;; The underscores here are by design - they show that these
;; are the low-level C functions.
;; The Lisp translations use dashes.
(cffi:defcstruct struct-svn-delta-editor
  (set_target_revision :pointer)
  (open_root :pointer)
  (delete_entry :pointer)
  (add_directory :pointer)
  (open_directory :pointer)
  (change_dir_prop :pointer)
  (close_directory :pointer)
  (absent_directory :pointer)
  (add_file :pointer)
  (open_file :pointer)
  (apply_textdelta :pointer)
  (change_file_prop :pointer)
  (close_file :pointer)
  (absent_file :pointer)
  (close_edit :pointer)
  (abort_edit :pointer)
  (apply_textdelta_stream :pointer))

(cffi:define-c-struct-wrapper struct-svn-delta-editor ())


(defvar *svn-delta-editor-data* nil)


(defstruct delta-editor-data
  (editor nil :type struct-svn-delta-editor)
  (e-baton)
  (e-pool)) ; apr-pool is not a CL type


(defun set-target-revision (num &key pool)
  (with-slots (editor e-baton e-pool) *svn-delta-editor-data*
    (with-apr-pool (p (or pool e-pool))
      (cffi:foreign-funcall-pointer (struct-svn-delta-editor-set_target_revision editor)
                                    ()
                                    :pointer e-baton
                                    :long    num
                                    :pointer p
                                    svn-error-type))))

(defun open-root (base-revision)
  "Open the root directory on the delta editor in *SVN-DELTA-EDITOR-DATA*;
  returns the root baton."
  (with-pointer-to-type/return (:pointer root-baton)
    (with-slots (editor e-baton e-pool) *svn-delta-editor-data*
      (cffi:foreign-funcall-pointer (struct-svn-delta-editor-open_root editor)
                                    ()
                                    :pointer e-baton
                                    :long    base-revision
                                    :pointer e-pool
                                    :pointer root-baton
                                    svn-error-type))))


(defun close-or-abort-edit (success)
  (with-slots (editor e-baton e-pool) *svn-delta-editor-data*
    (with-apr-pool (tmp-pool e-pool)
      (cffi:foreign-funcall-pointer (if success
                                      (struct-svn-delta-editor-close_edit editor)
                                  (struct-svn-delta-editor-abort_edit editor))
                                    ()
                                    :pointer e-baton
                                    apr-pool tmp-pool
                                    svn-error-type))))

;; =================== Files ===================

(defun add-a-file (path parent-baton &key copyfrom-path copyfrom-revnum)
  "Adds a new file PATH under PARENT-BATON."
  (with-pointer-to-type/return (:pointer file-baton)
    (with-slots (editor e-baton e-pool) *svn-delta-editor-data*
      (cffi:foreign-funcall-pointer (struct-svn-delta-editor-add_file editor)
                                    ()
                                    :string  path
                                    :pointer parent-baton
                                    :string  (or copyfrom-path (cffi:null-pointer))
                                    :long    (or copyfrom-revnum 0)
                                    apr-pool e-pool
                                    :pointer file-baton
                                    svn-error-type))))

(defun open-file (path parent-baton &key base-rev)
  "Opens a file."
  (with-pointer-to-type/return (:pointer file-baton)
    (with-slots (editor e-baton e-pool) *svn-delta-editor-data*
      (cffi:foreign-funcall-pointer (struct-svn-delta-editor-open_file editor)
                                    ()
                                    :string  path
                                    :pointer parent-baton
                                    :long    (or base-rev +SVN-IGNORED-REVNUM+)
                                    apr-pool e-pool
                                    :pointer file-baton
                                    svn-error-type))))

(defun apply-textdelta- (path parent-baton &key base-rev)
  "Opens a file."
  (with-pointer-to-type/return (:pointer file-baton)
    (with-slots (editor e-baton e-pool) *svn-delta-editor-data*
      (cffi:foreign-funcall-pointer (struct-svn-delta-editor-apply_textdelta editor)
                                    ()
                                    :string  path
                                    :pointer parent-baton
                                    :long    (or base-rev +SVN-IGNORED-REVNUM+)
                                    apr-pool e-pool
                                    :pointer file-baton
                                    svn-error-type))))



(defun close-file (baton &key checksum)
  "Closes the given file."
  (with-slots (editor e-baton e-pool) *svn-delta-editor-data*
    (with-apr-pool (pool e-pool)
      (cffi:foreign-funcall-pointer (struct-svn-delta-editor-close_file editor)
                                    ()
                                    :pointer baton
                                    :string  (or checksum (cffi:null-pointer))
                                    apr-pool pool
                                    svn-error-type))))

(defun change-file-prop (file-baton name value)
  "Changes property NAME to VALUE."
  (with-slots (editor e-pool) *svn-delta-editor-data*
    (with-apr-pool (pool e-pool)
      (cffi:foreign-funcall-pointer (struct-svn-delta-editor-change_file_prop editor)
                                    ()
                                    :pointer file-baton
                                    :string  (string name)
                                    :pointer (if value 
                                               ;; longer lifetime needed?
                                               (svn-string-create value pool)
                                               (cffi:null-pointer))
                                    apr-pool pool
                                    svn-error-type))))

;; =================== Directories ===================

(defun add-a-directory (path parent-baton &key copyfrom-path copyfrom-revnum)
  "Adds a new directory PATH under PARENT-BATON."
  (with-pointer-to-type/return (:pointer dir-baton)
    (with-slots (editor e-baton e-pool) *svn-delta-editor-data*
      (cffi:foreign-funcall-pointer (struct-svn-delta-editor-add_directory editor)
                                    ()
                                    :string  path
                                    :pointer parent-baton
                                    :string  (or copyfrom-path (cffi:null-pointer))
                                    :long    (or copyfrom-revnum 0)
                                    apr-pool e-pool
                                    :pointer dir-baton
                                    svn-error-type))))

(defun open-directory (path parent-baton &key base-rev)
  "Opens a directory."
  (with-pointer-to-type/return (:pointer dir-baton)
    (with-slots (editor e-baton e-pool) *svn-delta-editor-data*
      (cffi:foreign-funcall-pointer (struct-svn-delta-editor-open_directory editor)
                                    ()
                                    :string  path
                                    :pointer parent-baton
                                    :long    (or base-rev +SVN-IGNORED-REVNUM+)
                                    apr-pool e-pool
                                    :pointer dir-baton
                                    svn-error-type))))

(defun change-dir-prop (dir-baton name value)
  "Changes property NAME to VALUE."
  (with-slots (editor e-baton e-pool) *svn-delta-editor-data*
    (with-apr-pool (pool e-pool)
      (cffi:foreign-funcall-pointer (struct-svn-delta-editor-change_dir_prop editor)
                                    ()
                                    :pointer dir-baton
                                    :string  (string name)
                                    :pointer (if value 
                                               ;; longer lifetime needed?
                                               (svn-string-create value pool)
                                               (cffi:null-pointer))
                                    apr-pool pool
                                    svn-error-type))))

(defun close-directory (baton)
  "Closes the given directory baton."
  (with-slots (editor e-baton e-pool) *svn-delta-editor-data*
    (with-apr-pool (pool e-pool)
      (cffi:foreign-funcall-pointer (struct-svn-delta-editor-close_directory editor)
                                    ()
                                    :pointer baton
                                    apr-pool pool
                                    svn-error-type))))


(defmacro with-delta-editor-pool ((pool-var &key (editor '*svn-delta-editor-data*))
                                  &body body)
  "Binds POOL-VAR to the current editor's pool (useful for longer-lived allocations)."
  `(with-slots ((,pool-var e-pool)) ,editor
     ,@ body))
