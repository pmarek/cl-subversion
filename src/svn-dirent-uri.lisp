(in-package :cl-subversion/api)


(cffi:defcfun ("svn_dirent_canonicalize" svn_dirent_canonicalize%) :string
  (path :string)
  (pool apr-pool))

(defun svn-dirent-canonicalize (path pool)
  (svn_dirent_canonicalize% (if (pathnamep path)
                                (namestring path)
                                path)
                            pool))

(cffi:defcfun ("svn_uri_canonicalize" svn-uri-canonicalize) :string
  (path :string)
  (pool apr-pool))

