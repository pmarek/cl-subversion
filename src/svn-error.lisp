(in-package :cl-subversion/api)


(cffi:defcfun ("svn_err_best_message" svn_err_best_message%) :string
  (err :pointer)
  (buf :string)
  (bufsize :int))


(defun svn-err-best-message (error-struct &key (max-size 1024))
  (let ((str (cffi:foreign-alloc :char 
                                 :count max-size
                                 :initial-element 0)))
    (svn_err_best_message% error-struct
                           str
                           (1- max-size))))


(defmacro svn-error-case (form &rest cases)
  (alexandria:with-gensyms (error)
    `(let ((,error ,form))
       (cond
         ((cffi:null-pointer-p ,error)
          T)
         (T
          (error "~&svn error encountered:~% ~s"
                 (svn-err-best-message ,error))
          ,@ cases)))))


(cffi:define-foreign-type svn-error-type ()
   nil
   (:actual-type :pointer)
   (:simple-parser svn-error-type))


(defmethod cffi:translate-from-foreign (rv (type svn-error-type))
  (if (not (cffi:null-pointer-p rv))
    (error "~&SVN error encountered:~% ~s"
           (svn-err-best-message rv))
    rv))
