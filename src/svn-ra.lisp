(in-package :cl-subversion/api)


(defvar *svn-callback-table* nil)


(cffi:defcfun ("svn_ra_initialize" svn-ra-initialize) svn-error-type
  (pool apr-pool))

(cffi:defcstruct svn_ra_callbacks2_t
	(open_tmp_file :pointer)
	(auth_baton :pointer)
	(get_wc_prop :pointer)
	(set_wc_prop :pointer)
	(push_wc_prop :pointer)
	(invalidate_wc_props :pointer)
	(progress_func :pointer)
	(progress_baton :pointer)
	(cancel_func :pointer)
	(get_client_string :pointer)
	(get_wc_contents :pointer)
	(check_tunnel_func :pointer)
	(open_tunnel_func :pointer)
	(tunnel_baton :pointer))


(cffi:defcfun ("svn_ra_create_callbacks" svn_ra_create_callbacks%) svn-error-type
  (callbacks :pointer)
  (pool apr-pool))

(defun svn-ra-create-callbacks (pool)
  (with-pointer-to-type/return (:pointer cb)
    (svn_ra_create_callbacks% cb pool)))
 

(cffi:defcfun ("svn_ra_open4" svn_ra_open4%) svn-error-type
  (session_p :pointer)
  (corrected_url :pointer)
  (repos_URL :string)
  (uuid :string)
  (callbacks :pointer)
  (callback_baton :pointer)
  (config :pointer)
  (pool apr-pool))

(defun svn-ra-open (url pool &key
                        uuid
                        (callbacks nil)
                        (callback-baton (cffi:null-pointer))
                        config)
  "Opens a new RA session to URL, using POOL,
  optionally verifying the repository UUID.
  CONFIG can be an apr-hash of configuration settings.
  Returns the new session."
  ;; Sadly APR-HASH and APR-POOL are not CL types that can be checked...
  (check-type config (or null cffi:foreign-pointer))
  (check-type uuid (or null string))
  (check-type pool cffi:foreign-pointer)
  (with-pointer-to-type/return (:pointer session)
    (svn_ra_open4% session
                   (cffi:null-pointer)
                   url
                   (or uuid (cffi:null-pointer))
                   (or callbacks 
                       *svn-callback-table*)
                   callback-baton
                   (or config (cffi:null-pointer))
                   pool)))


(cffi:defcfun ("svn_ra_get_latest_revnum" svn_ra_get_latest_revnum%) svn-error-type
  (session :pointer)
  (latest_revnum (:pointer :long))
  (pool apr-pool))


(defun svn-ra-get-latest-revnum (session pool)
  "Returns the revision number"
  (with-pointer-to-type/return (:long revnum)
    (svn_ra_get_latest_revnum% session revnum pool)))


(cffi:defcstruct svn_ra_callbacks_t
    (open_tmp_file :pointer)
    (auth_baton :pointer)
    (get_wc_prop :pointer)
    (set_wc_prop :pointer)
    (push_wc_prop :pointer)
    (invalidate_wc_props :pointer))



(cffi:defcfun ("svn_ra_get_commit_editor3" svn_ra_get_commit_editor3%) svn-error-type
  (session :pointer)
  (editor (:pointer :pointer))
  (edit_baton (:pointer :pointer))
  (revprop_hash :pointer)
  (callback :pointer)
  (commit_baton :pointer)
  (lock_tokens :pointer)
  (keep_locks :int)
  (pool apr-pool))


(defun svn-ra-get-commit-editor3 (session revprops pool &key
                                          (callback (lambda (new-rev) )))
  "Using SESSION, starts a new commit, sending REVPROPS in;
  returns an internal structure with required data for further calls.
  The author can't be set via the revision properties; see "
  (declare (ignore callback))
  (with-pointer-to-type (:pointer editor-sap :getter g-editor-sap)
    (with-pointer-to-type (:pointer edit-baton :getter g-baton)
      (svn_ra_get_commit_editor3% session
                                  editor-sap edit-baton
                                  (hash-to-aprhash revprops pool)
                                  (cffi:null-pointer) ; (cffi:callbackaaaaa   callback)
                                  (cffi:null-pointer)
                                  (apr-hash-make pool) ; locks
                                  0 ; keep locks
                                  pool)
      (let* ((editor-class (make-instance 'struct-svn-delta-editor
                                          :pointer (g-editor-sap))))
        (make-delta-editor-data :editor editor-class
                                :e-baton (g-baton)
                                :e-pool  pool)))))


(defmacro with-svn-ra-commit-editor ((session pool revprops &key
                                              block-name
                                              (call-close 'funcall))
                                     &body body)
  "Starts a commit session; within BODY the special *SVN-DELTA-EDITOR-DATA*
  is bound but can be switched as required.
  Abort the commit by a nonlocal transfer or via RETURNing from BLOCK-NAME."
  (alexandria:with-gensyms (success) 
    `(let ((,success nil)
           (*svn-delta-editor-data* 
             (svn-ra-get-commit-editor3 ,session ,revprops ,pool)))
       (unwind-protect
           (block ,block-name
                  (prog1
                      (progn 
                        ,@ body)
                    (setf ,success t)))
         ;; after BODY
         (,call-close (lambda ()
                        (close-or-abort-edit ,success)))))))

(cffi:defcfun ("svn_ra_get_file" svn_ra_get_file%) svn-error-type
  (session :pointer)
  (path :string)
  (revision :long)
  (stream :pointer)
  (fetched-rev (:pointer :long))
  (properties (:pointer apr-hash))
  (pool apr-pool))

 
(defun svn-ra-get-file (session path revision consumer &optional pool)
  "Pushes pieces of PATH at REVISION to CONSUMER;
  returns the properties and, if REVISION was
  +SVN-IGNORED-REVNUM+ (which means HEAD) the fetched
  revision number.
  CONSUMER might be a file path (written in binary mode), a stream,
  a lambda that receives UB8 vectors (and a NIL as EOF),
  or NIL if the actual file data is not needed."
  ;; TODO: methods for CONSUMER being STREAM, LAMBDA, VECTOR, STRING?
  (cl-subversion/apr:with-apr-pool (local-pool pool)
    (cl-subversion/ffi:with-pointer-to-type ('apr-hash props :getter get-props)
      (cl-subversion/ffi:with-pointer-to-type (:long revnum :getter get-revnum)
        (multiple-value-bind (stream cleanup-fn)
            (make-a-writable-svn-stream consumer local-pool)
          (unwind-protect
              (progn
                (svn_ra_get_file% session path
                                  (or revision +SVN-IGNORED-REVNUM+)
                                  stream
                                  revnum props
                                  local-pool)
                (values (aprhash-to-alist (get-props) local-pool)
                        (get-revnum)))
            (if cleanup-fn
                (funcall cleanup-fn))))))))


(defconstant +svn-dirent-all+           #xffffffff)
(defconstant +svn_dirent_kind+          #x00000001)
(defconstant +svn_dirent_size+          #x00000002)
(defconstant +svn_dirent_has_props+     #x00000004)
(defconstant +svn_dirent_created_rev+   #x00000008)
(defconstant +svn_dirent_time+          #x00000010)
(defconstant +svn_dirent_last_author+   #x00000020)

(cffi:defcenum svn_node_kind_t
    :svn-node-none
    :svn-node-file
    :svn-node-dir
    :svn-node-unknown
    :svn-node-symlink)


(cffi:defcfun ("svn_ra_get_dir2" svn_ra_get_dir%) svn-error-type
  (session :pointer)
  (dirents (:pointer (:pointer apr-hash)))
  (fetched-rev (:pointer :long))
  (props (:pointer apr-hash))
  (path :string)
  (rev :long)
  (fields :long)
  (pool apr-pool))

(cffi:defcstruct struct-svn-dirent
  (kind svn_node_kind_t)
  (size :int64)
  (has-props :bool)
  (created-rev :uint64)
  (unix-usecs :uint64)
  (last-author :string))

(cffi:define-c-struct-wrapper (svn-dirent (:struct struct-svn-dirent)) ())

(defun get-svn-dirent (sap)
  (make-instance 'svn-dirent
                 :pointer sap))

(defun svn-ra-get-dir (session path &key 
                               (rev +svn-ignored-revnum+)
                               (fields +svn-dirent-all+)
                               (want-dirents t)
                               (want-props t)
                               pool)
  "Fetches information about PATH at REV in SESSION.
  Returns multiple values: the entries and the properties as svn-hash,
  and the fetched revision number."
  (cl-subversion/apr:with-apr-pool (local-pool pool)
    (cl-subversion/ffi:with-pointer-to-type ('apr-hash props :getter get-props)
      (cl-subversion/ffi:with-pointer-to-type (:pointer ents-ptr :getter get-ents-ptr :data entries)
        (cl-subversion/ffi:with-pointer-to-type (:long revnum :getter get-revnum)
          (svn_ra_get_dir% session
                           (if want-dirents 
                               ents-ptr
                               (cffi:null-pointer))
                           revnum
                           (if want-props 
                               props
                               (cffi:null-pointer))
                           path
                           rev
                           fields
                           local-pool)
          (values (if want-dirents
                      (aprhash-to-alist (get-ents-ptr) local-pool
                                        :value-builder #'get-svn-dirent))
                  (aprhash-to-alist (get-props) local-pool)
                  (get-revnum)))))))


(defun is-svn-property (name)
  "Returns a true value if NAME specifies an svn-internal property name."
  (alexandria:starts-with-subseq "svn:" name))


(cffi:defcfun ("svn_ra_rev_proplist" svn_ra_rev_proplist%) svn-error-type
  (session :pointer)
  (rev :long)
  (properties (:pointer (:pointer apr-hash)))
  (pool apr-pool))


(defun svn-ra-rev-proplist (session revision pool)
  "Fetches information about PATH at REV in SESSION.
  Returns multiple values: the entries and the properties as svn-hash,
  and the fetched revision number."
  (cl-subversion/apr:with-apr-pool (local-pool pool)
    (cl-subversion/ffi:with-pointer-to-type ('apr-hash props :getter get-props)
      (svn_ra_rev_proplist% session
                            revision
                            props
                            local-pool)
      (aprhash-to-alist (get-props) local-pool))))
