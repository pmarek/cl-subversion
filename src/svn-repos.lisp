(in-package :cl-subversion/api)

(cffi:defcfun ("svn_fs_initialize" svn-fs-initialize) svn-error-type
  (pool apr-pool))


(cffi:defcfun ("svn_repos_create" svn-repos-create%) svn-error-type
  (repos-p (:pointer :pointer))
  (path :string)
  (unused1 :string)
  (unused2 :string)
  (client-config apr-hash)
  (fs-config apr-hash)
  (pool apr-pool))


(alexandria:define-constant +fs-type+ "fs-type" :test #'equal)

(alexandria:define-constant +fs-type-bdb+  "bdb"  :test #'equal)
(alexandria:define-constant +fs-type-fsx+  "fsx"  :test #'equal)
(alexandria:define-constant +fs-type-fsfs+ "fsfs" :test #'equal)




(defun svn-repos-create (path &key pool fs-config client-config)
  "Creates a new SVN repository at PATH.
  FS-CONFIG can contain eg. +FS-TYPE+."
  (with-pointer-to-type/return (:pointer repos)
    (cl-subversion/apr:with-apr-pool (scratch-pool pool)
      (svn-repos-create% repos 
                         (svn-dirent-canonicalize path scratch-pool)
                         (cffi:null-pointer)
                         (cffi:null-pointer)
                         (hash-to-aprhash client-config scratch-pool)
                         (hash-to-aprhash fs-config scratch-pool)
                         scratch-pool))))
