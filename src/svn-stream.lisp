(in-package :cl-subversion/api)


(cffi:defctype svn-stream-t :pointer)


(cffi:defcfun ("svn_stream_create" svn_stream_create) svn-stream-t
  (baton :pointer)
  (pool apr-pool))

(cffi:defcfun ("svn_stream_set_write" svn_stream_set_write) :void
  (stream svn-stream-t)
  (write-fn :pointer))

 

;; The address of anything may change after a GC, so use a counter
(defvar *my-baton-closure-id* (cons 1 0))
(defvar *my-baton-closures* (make-hash-table :synchronized t))


(defun set-closure-in-baton (id function)
  "Arranges for BODY to be callable via ID."
  (if (gethash id *my-baton-closures*)
      (error "ID ~s already used" id))
  (setf (gethash id *my-baton-closures*)
        function))

(defun clear-closure (id)
  (remhash id *my-baton-closures*))

(defun call-closure (baton &rest args)
  (apply (gethash (cffi:pointer-address baton)
                  *my-baton-closures*)
         args))


(cffi:defcallback svn-stream-write-cb :pointer
    ((baton :pointer) (data :pointer) (len :pointer)) 
  (call-closure baton data 
                (cffi:mem-ref len :long))
  (cffi:null-pointer))


(defgeneric writeable-svn-stream-lambda (dest &rest options)
  (:documentation
    "Allocates an svn_stream_t that pushes data to DEST."))


(defmethod writeable-svn-stream-lambda ((dest cons) &rest options)
  ;; This is the method that acts on options in DEST.
  (assert (null options) ()
          "INTERNAL LOGIC ERROR - This method can't get any options.")
  (destructuring-bind (i-dest &rest i-options) dest
    (assert (not (consp i-dest)) ()
            "Only a single options layer")
    ;; Use the provided options
    (apply #'writeable-svn-stream-lambda i-dest i-options)))


(defmethod writeable-svn-stream-lambda ((dest function) &rest options)
  (values
    (if (getf options :want-sap)
        dest
        (lambda (data len)
          (let ((vec (make-array len 
                                 :element-type (getf options :element-type 
                                                     '(unsigned-byte 8)))))
            (dotimes (i len)
              (setf (aref vec i)
                    (cffi:mem-ref data :unsigned-char i)))
            (funcall dest vec))))
    (lambda ()
      (funcall dest nil))))


(defmethod writeable-svn-stream-lambda ((dest pathname) &rest options)
  (let ((stream (apply #'open dest  
                       :direction :output
                       :element-type (getf options :element-type '(unsigned-byte 8))
                       ;; Any other options
                       options)))
    (multiple-value-bind (writer cleanup)
        (writeable-svn-stream-lambda stream)
      (values writer
              (lambda ()
                (if cleanup
                    (funcall cleanup))
                (close stream))))))


(defmethod writeable-svn-stream-lambda ((dest stream) &rest options)
  (assert (null options) ()
          "This method takes no options.")
  (lambda (data len)
    (block nil 
      #+sbcl (when (typep dest 'sb-sys:fd-stream)
               ;; SBCL only, and only for SB-SYS:FD-STREAMs
               (return
                 (sb-impl::buffer-output dest data 0 len)))
      (dotimes (i len)
        (princ (code-char (cffi:mem-ref data :unsigned-char i)) 
               dest)))))


(defun make-a-writable-svn-stream (dest pool)
  (cond
    ((null dest)
     ;; Useful to get just the properties and latest revision number
     (cffi:null-pointer))
    (t
     (let* ((id (sb-ext:atomic-incf (car *my-baton-closure-id*)))
            (svn-stream (svn_stream_create (cffi:make-pointer id)
                                           pool)))
       (multiple-value-bind (writer-fn cleanup-fn)
           (writeable-svn-stream-lambda dest)
         (set-closure-in-baton id writer-fn)
         (svn_stream_set_write svn-stream
                               (cffi:get-callback 'svn-stream-write-cb))
         (values svn-stream
                 cleanup-fn))))))
