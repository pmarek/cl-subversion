(in-package :cl-subversion/ffi)

(cffi:defcstruct (svn_string_t
                   :conc-name svn-string-)
	(data :string)
	(len :uint64))


;; We could create the svn_string_t structures easily ourselves --
;; but then they'd needed pinning and other lifetime management.
;; Should be easier to just allocate them in the same pool that
;; is used in the function consuming them.
(cffi:defcfun ("svn_string_create" svn-string-create) :pointer
  (cstring :string)
  (pool :pointer))

;; Is there a cleaner way than defining the same function again?
(cffi:defcfun ("svn_string_ncreate" svn-string-ncreate-from-ub8%) :pointer
  (ub8 :pointer)
  (len :uint64)
  (pool :pointer))

(defun svn-string-create-from-ub8 (bytes pool &key len)
  (cffi:with-pointer-to-vector-data (ub8 bytes) 
    (svn-string-ncreate-from-ub8% ub8 
                                 (or len
                                     (length bytes))
                                 pool)))

(defun svn-string-to-lisp (svn-string)
  (svn-string-data svn-string))
