(in-package :cl-subversion/api)


(defvar *global-apr-pool* nil)


(defun only-svn-properties (alist)
  (remove-if-not #'is-svn-property
                 alist
                 :key #'car))

(defun only-user-properties (alist)
  (remove-if #'is-svn-property
             alist
             :key #'car))


(cffi:defcfun ("svn_cmdline_create_auth_baton2" svn_cmdline_create_auth_baton%) svn-error-type
  (auth_baton :pointer)
  (non_interactive :boolean)
  (username :string)
  (password :string)
  (cfg_dir :string)
  (no_auth_cache :boolean)
  (trust_server_cert_unknown_ca :boolean)
  (trust_server_cert_cn_mismatch :boolean)
  (trust_server_cert_expired :boolean)
  (trust_server_cert_not_yet_valid :boolean)
  (trust_server_cert_other_failure :boolean)
  (svn_config :pointer)
  (cancel_func :pointer)
  (cancel_baton :pointer)
  (apr_pool_t apr-pool))

(defun svn-cmdline-create-auth-baton (username pool 
                                               &key (interactive nil) password
                                               cfg-dir auth-cache config)
  (assert *svn-callback-table*)
  (let ((cfg-data (or config
                      (svn-config-get-config :sub-section "config"))))
    (with-pointer-to-type/return (:pointer baton get-auth-baton)
      (svn_cmdline_create_auth_baton% baton
                                      (not interactive)
                                      (cl-subversion/apr:apr-pstrdup-as-pointer 
                                        pool
                                        (or username "CL-USER"))
                                      (cl-subversion/apr:apr-pstrdup-as-pointer 
                                        pool
                                        (or password "CL-P"))
                                      (or cfg-dir (cffi:null-pointer))
                                      (not auth-cache)
                                      0 0 0 0 0 ; TODO
                                      (or cfg-data (cffi:null-pointer))
                                      (cffi:null-pointer)
                                      (cffi:null-pointer)
                                      pool)
      ;; TODO: thread-specific initialization?
      (setf (cffi:foreign-slot-value *svn-callback-table* 
                                     '(:struct svn_ra_callbacks2_t)
                                     'auth_baton)
            (get-auth-baton)))))



(defun initialize ()
  (cffi:use-foreign-library "libapr-1.so")
  (cffi:use-foreign-library "libsvn_ra-1.so")
  (let ((apr-status (cl-subversion/apr:apr-initialize)))
    (unless (zerop apr-status)
      (error "Couldn't initialize libapr, got error code ~d" apr-status)))
  ;;
  (if (null *global-apr-pool*)
    (setf *global-apr-pool*
          (cl-subversion/apr:apr-pool-create)))
  (svn-ra-initialize *global-apr-pool*)
  ;; TODO: only if needed?
  (svn-fs-initialize *global-apr-pool*)
  ;;
  (setf *svn-callback-table*
        (svn-ra-create-callbacks *global-apr-pool*))
  ;;
  (svn-cmdline-create-auth-baton nil *global-apr-pool*)
  ;;
  T)


;; Ensure that we're good to go... for restored images
;; the user has to run that himself
(initialize)


(cffi:defcfun ("svn_config_get_config" svn-config-get-config%) svn-error-type
  (cfg-hash (:pointer apr-hash))
  (config_dir :string) 
  (pool apr-pool))


(defun svn-config-get-config (&key config-dir sub-section)
  "Fetches the configuration from default paths.
  POOL should be a global pool!"
  (let ((cfg-data (with-pointer-to-type/return (:pointer cfg)
                    (svn-config-get-config% cfg
                                            (or config-dir (cffi:null-pointer))
                                            *global-apr-pool*))))
    (if sub-section
        (apr-hash-get cfg-data sub-section)
        cfg-data)))
