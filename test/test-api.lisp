(in-package :svn-test/api)


(def-suite :svn-api)

(fiveam:def-test commit-via-api-functions ()
  (with-temp-repo (:pool-sym pool)
    (format t "~&Testing against ~s; ~s~%" *repo-dir* *repo-url*)
    (svn-cmdline-create-auth-baton "User" pool
                                   :config (cffi:null-pointer))
    (let ((session (svn/api:svn-ra-open *repo-url* pool)))
      (let ((base-rev (svn/api:svn-ra-get-latest-revnum session pool)))
        (svn/api:with-svn-ra-commit-editor (session 
                                             pool
                                             `(("svn:log" . "log1")
                                               ("foobar" . "baz")
                                               ))
          (let ((root-baton (svn/api:open-root base-rev)))
            (let ((dir-baton (svn/api:add-a-directory "dir1" root-baton)))
              (svn/api:change-dir-prop dir-baton "foo" "blub")
              (svn/api:close-directory 
                (svn/api:add-a-directory "dir1/dirX" dir-baton))
              (svn/api:close-directory dir-baton))
            (let ((file-baton (svn/api:add-a-file "file+" root-baton)))
              (let* ((txdelta-handler (svn/api:apply-textdelta file-baton pool))
                     (txdelta-window (svn/api:make-window-with-one-copy-op 
                                       "awful" 5 0
                                       pool)))
                (svn/api:call-txdelta-handler-with-window txdelta-handler txdelta-window)
                (svn/api:call-txdelta-handler-with-window txdelta-handler nil))
              (svn/api:change-file-prop file-baton "file" "prop")
              (svn/api:close-file file-baton))
            (svn/api:close-directory root-baton)))))
    (let* ((session (svn/api:svn-ra-open *repo-url* pool)))
      (multiple-value-bind (ents props rev)
          (svn/api:svn-ra-get-dir session 
                                  "dir1" 
                                  :want-dirents t
                                  :pool pool)
        (is (= rev 1))
        (is (equalp `(("foo" . "blub"))
                    (svn/api:only-user-properties props)))
        (is (string= "User"
                     (alexandria:assoc-value props "svn:entry:last-author" 
                                             :test #'string=)))
        ;;
        (is (= 1 (length ents)))
        (destructuring-bind ((name . ent)) ents
          (is (string= "dirX" name))
          (is (eq :svn-node-dir (svn/api::svn-dirent-kind ent)))
          (is (eq nil           (svn/api::svn-dirent-has-props ent)))
          (is (=  1             (svn/api::svn-dirent-created-rev ent)))
          (is (string= "User"   (svn/api::svn-dirent-last-author ent))))
        ;;
        (let ((revprops (svn-ra-rev-proplist session
                                             rev
                                             pool)))
          (flet ((rp (x)
                   (alexandria:assoc-value revprops x :test #'string=)))
            (is (string= "baz"  (rp "foobar")))
            (is (string= "User" (rp "svn:author")))
            (is (string= "log1" (rp "svn:log")))))
        ;;
        (with-output-to-string (data)
          (multiple-value-bind (file-props file-rev) 
              (svn/api:svn-ra-get-file session "file+" nil data)
            (is (= 1 file-rev))
            (is (equalp `(("file" . "prop"))
                        (only-user-properties file-props))
                (is (string= "awful"
                             (get-output-stream-string data))))))))))
