(in-package :svn-test/api)

(defparameter *keep-temp-dirs* nil
  "Can be set to T to keep the created repositories for debugging.")


(defparameter *repo-dir* nil)
(defparameter *repo-url* nil)

(svn:initialize)


(defmacro with-temp-dir ((dir-sym) &body body)
  (alexandria:with-gensyms (tmpdir)
    `(let* ((,tmpdir (osicat-posix:mkdtemp
                       (format nil "/~a/cl-svn.tmp."
                               osicat:*temporary-directory*
                               )))
            (,dir-sym (probe-file ,tmpdir)))
       (unwind-protect
           (progn . , body)
         (unless *keep-temp-dirs*
           ;; Safety check - / tmp / XXXXXX
           (if (< (length ,tmpdir) (+ 1 3 1 6))
               (error "Potentially unsafe delete operation on ~s" ,dir-sym)
               (ql-impl-util:delete-directory-tree ,dir-sym)))))))


(defmacro with-temp-repo ((&key (pool-sym (gensym "POOL-")))
                          &body body)
  "Provides a repository at a temp path, and binds *REPO-DIR* and *REPO-URL*."
  `(with-temp-dir (*repo-dir*)
     (cl-subversion/apr:with-apr-pool (,pool-sym)
       (let ((*repo-url* (svn/api:svn-uri-canonicalize
                           (format nil "file://~a" *repo-dir*)
                           ,pool-sym)))
         (svn-repos-create *repo-dir*
                           ;:fs-config `((,svn/api:+fs-type+ . ,svn:+fs-type-fsx+))
                           :pool ,pool-sym)
         ,@ body))))


(setf fiveam:*on-error* :debug
      fiveam:*run-test-when-defined* t)

