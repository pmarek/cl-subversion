(in-package :svn-test/high-level)

(fiveam:def-suite :svn-hl)

(def-test commit-via-hl-1 ()
  (with-temp-repo (:pool-sym pool)
    (with-svn-commit (*repo-url* :root "."
                                 :log-text "High5"
                                 :parent-pool pool)
      (with-added-directory ("foo")
        (add-file "baz"
                  "my-data"
                  :props `((:file-prop . "huzzah!")))
        (with-added-directory ("bar")
          (change-directory-properties `((:bar-prop . "42"))))))
    (format t "going to ~a~%" *repo-url*)
    (with-svn-ra-session (*repo-url* pool)
      (multiple-value-bind (content prop rev)
          (get-file-as-string "foo/baz")
        (is (= 1 rev))
        (is (string= "my-data" content))
        (is (equalp `(("file-prop". "huzzah!"))
                    (only-user-properties prop)))))))
