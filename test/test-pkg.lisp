(cl:defpackage :svn-test/api
  (:use :cl
        :svn/api
        :fiveam)
  (:export #:with-temp-dir
           #:with-temp-repo
           #:*repo-dir*
           #:*repo-url*
           ))

(cl:defpackage :svn-test/high-level
  (:use :cl 
        :cl-subversion/high-level 
        :svn-test/api
        :fiveam))


